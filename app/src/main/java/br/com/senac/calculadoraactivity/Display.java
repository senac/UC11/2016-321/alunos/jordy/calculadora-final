package br.com.senac.calculadoraactivity;

import android.widget.TextView;

/**
 * Created by sala304b on 25/08/2017.
 */

public class Display {

    private TextView textView;
    private boolean limpaDisplay = true;

    public Display(TextView display) {
        this.textView = display;
    }

    public TextView getTextView() {
        return textView;
    }


    public boolean isLimpaDisplay() {
        return limpaDisplay;
    }

    public void setLimpaDisplay(boolean limpaDisplay) {
        this.limpaDisplay = limpaDisplay;
    }

    public String getText() {
        return this.textView.getText().toString();
    }

    public void setText(String texto) {
        this.textView.setText(texto);
    }

    public void setText(double valor) {
        this.setText(String.valueOf(valor));
    }


    public double getValue() {
        return Double.parseDouble(this.getText());
    }
}

